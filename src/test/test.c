#include "include.h"
#include "test.h"
//#include "../gui/pic.h"
//========================variable==========================//
u16 ColorTab[5]={RED,GREEN,BLUE,YELLOW,BRED};//定义颜色数组
//=====================end of variable======================//

/*****************************************************************************
 * @name       :void DrawTestPage(u8 *str)
 * @date       :2018-08-09 
 * @function   :Drawing test interface
 * @parameters :str:the start address of the Chinese and English strings
 * @retvalue   :None
******************************************************************************/ 

void test_gui(void){
          LCD_Clear(WHITE);
          //English_Font_test();
          LCD_Clear(YELLOW);
          LCD_Fill(0,0,80,80,RED);
          LCD_Fill(0,80,80,160,GREEN);
          Show_Str(10,20,WHITE,YELLOW,"24V 100%",16,1);
          Show_Str(10,40,WHITE,YELLOW,"  20Km/h",16,1);
          Show_Str(10,60,WHITE,YELLOW,"    STOP",16,1);
          //LCD_ShowNum(10,80,8,50,1);
          Gui_DrawBattery(10,100, 80);
}

void DrawTestPage(u8 *str)
{
  LCD_Clear(WHITE);
  LCD_Fill(0,0,lcddev.width,20,BLUE);
//绘制固定栏down
//LCD_Fill(0,lcddev.height-20,lcddev.width,lcddev.height,BLUE);
POINT_COLOR=WHITE;
Gui_StrCenter(0,2,WHITE,BLUE,str,16,1);//居中显示
//Gui_StrCenter(0,lcddev.height-18,WHITE,BLUE,"www.lcdwiki.com",16,1);//居中显示
//绘制测试区域
//LCD_Fill(0,20,lcddev.width,lcddev.height-20,WHITE);
}

/*****************************************************************************
 * @name       :void Display_ButtonUp(u16 x1,u16 y1,u16 x2,u16 y2)
 * @date       :2018-08-24 
 * @function   :Drawing a 3D button
 * @parameters :x1:the bebinning x coordinate of the button
                y1:the bebinning y coordinate of the button
								x2:the ending x coordinate of the button
								y2:the ending y coordinate of the button
 * @retvalue   :None
******************************************************************************/ 
void Display_ButtonUp(u16 x1,u16 y1,u16 x2,u16 y2)
{
	POINT_COLOR=WHITE;
	LCD_DrawLine(x1,  y1,  x2,y1); //H
	LCD_DrawLine(x1,  y1,  x1,y2); //V
	
	POINT_COLOR=GRAY1;
	LCD_DrawLine(x1+1,y2-1,x2,y2-1);  //H
	POINT_COLOR=GRAY2;
	LCD_DrawLine(x1,y2,x2,y2);  //H
	POINT_COLOR=GRAY1;
	LCD_DrawLine(x2-1,y1+1,x2-1,y2);  //V
	POINT_COLOR=GRAY2;
  LCD_DrawLine(x2,y1,x2,y2); //V
}

/*****************************************************************************
 * @name       :void menu_test(void)
 * @date       :2018-08-24 
 * @function   :Drawing a 3D menu UI
 * @parameters :None
 * @retvalue   :None
******************************************************************************/ 
void menu_test(void)
{
	LCD_Clear(GRAY0);
	if(lcddev.dir==1||lcddev.dir==3)
	{
		Display_ButtonUp(27,1,133,21); //x1,y1,x2,y2
		Gui_StrCenter(0,2,BRED,BLUE,"图形显示测试",16,1);

		Display_ButtonUp(27,29,133,49); //x1,y1,x2,y2
		Gui_StrCenter(0,30,BROWN,GRAY0,"纯色填充测试",16,1);
	
		Display_ButtonUp(27,58,133,78); //x1,y1,x2,y2
		Gui_StrCenter(0,59,BLUE,GRAY0,"中文显示测试",16,1);
	}
	else
	{
		Display_ButtonUp(2,25,77,45); //x1,y1,x2,y2
		Gui_StrCenter(0,26,BRED,BLUE,"图形显示",16,1);

		Display_ButtonUp(2,55,77,75); //x1,y1,x2,y2
		Gui_StrCenter(0,56,BROWN,GRAY0,"纯色填充",16,1);
	
		Display_ButtonUp(2,85,77,105); //x1,y1,x2,y2
		Gui_StrCenter(0,86,BLUE,GRAY0,"中文显示",16,1);
		
		Display_ButtonUp(2,115,77,135); //x1,y1,x2,y2
		Gui_StrCenter(0,116,RED,GRAY0,"图片显示",16,1);
	}
	delay_ms(1500);
	delay_ms(500);
}

/*****************************************************************************
 * @name       :void main_test(void)
 * @date       :2018-08-09 
 * @function   :Drawing the main Interface of the Comprehensive Test Program
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void main_test(void)
{
//	DrawTestPage("综合测试程序");	
//	Gui_StrCenter(0,23,RED,BLUE,"全动电子",16,1);//居中显示
	LCD_Clear(WHITE);
	if(lcddev.dir==1||lcddev.dir==3)
	{
		Gui_StrCenter(0,0,RED,BLUE,"全动电子",16,1);//居中显示
		Gui_StrCenter(0,16,RED,BLUE,"综合测试程序",16,1);//居中显示	
		Gui_StrCenter(0,32,GREEN,BLUE,"0.96\" IPS ST7735S",16,1);//居中显示
		Gui_StrCenter(0,48,BLACK,BLUE,"80X160",16,1);//居中显示
		Gui_StrCenter(0,64,BLUE,BLUE,"2019-07-08",16,1);//居中显示
	}
	else
	{
		Gui_StrCenter(0,32,RED,BLUE,"全动电子",16,1);//居中显示
		Gui_StrCenter(0,48,RED,BLUE,"综合测试",16,1);//居中显示	
		Gui_StrCenter(0,64,GREEN,BLUE,"0.96\" IPS",16,1);//居中显示
		Gui_StrCenter(0,80,BLACK,BLUE,"ST7735S",16,1);//居中显示
		Gui_StrCenter(0,96,BROWN,BLUE,"80x160",16,1);//居中显示
		Gui_StrCenter(0,112,BLUE,BLUE,"2019-07-08",16,1);//居中显示
	}
	delay_ms(1500);		
	delay_ms(1500);
}

/*****************************************************************************
 * @name       :void Test_Color(void)
 * @date       :2018-08-09 
 * @function   :Color fill test(white,black,red,green,blue)
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Test_Color(void)
{
	//DrawTestPage("测试1:纯色填充测试");
	LCD_Fill(0,0,lcddev.width,lcddev.height,WHITE);
	Show_Str(20,30,BLUE,YELLOW,"BL Test",16,1);delay_ms(800);
	LCD_Fill(0,0,lcddev.width,lcddev.height,RED);
	Show_Str(20,30,BLUE,YELLOW,"RED ",16,1);delay_ms(800);
	LCD_Fill(0,0,lcddev.width,lcddev.height,GREEN);
	Show_Str(20,30,BLUE,YELLOW,"GREEN ",16,1);delay_ms(800);
	LCD_Fill(0,0,lcddev.width,lcddev.height,BLUE);
	Show_Str(20,30,RED,YELLOW,"BLUE ",16,1);delay_ms(800);
}

/*****************************************************************************
 * @name       :void Test_FillRec(void)
 * @date       :2018-08-09 
 * @function   :Rectangular display and fill test
								Display red,green,blue,yellow,pink rectangular boxes in turn,
								1500 milliseconds later,
								Fill the rectangle in red,green,blue,yellow and pink in turn
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Test_FillRec(void)
{
 
  u8 i=0;
  //_printf("lcddev.height: %d\r\n",lcddev.height);
   if(lcddev.dir==1||lcddev.dir==3)
  {
    LCD_Clear(WHITE);
    delay_ms(1);
    //LCD_Fill(0,0,160,lcddev.height-1,BLUE);
    LCD_Fill(0,0,160,79,BLUE);
    delay_ms(1);
    POINT_COLOR=WHITE;
    Show_Str(0,0,WHITE,BLUE,"1234",16,2);
    //Show_Str(2,24,WHITE,BLUE,"形",16,1);
    //Show_Str(2,40,WHITE,BLUE,"填",16,1);
    Show_Str(2,56,WHITE,BLUE,"ABCD",16,1);

  }
  else
  {
    DrawTestPage("矩形填充");
  }
  delay_ms(10);
  

  for (i=0; i<5; i++) 
  {
          POINT_COLOR=ColorTab[i];
          LCD_DrawRectangle(lcddev.width/2-38+(i*11),lcddev.height/2-38+(i*11),lcddev.width/2-38+(i*11)+30,lcddev.height/2-38+(i*11)+30); 
  }
  delay_ms(1500);	
  if(lcddev.dir==1||lcddev.dir==3)
  {
          LCD_Fill(21,0,lcddev.width-1,lcddev.height-1,WHITE);
  }
  else
  {
          LCD_Fill(0,21,lcddev.width-1,lcddev.height-1,WHITE);
  }
  for (i=0; i<5; i++) 
  {
          POINT_COLOR=ColorTab[i];
          LCD_DrawFillRectangle(lcddev.width/2-38+(i*11),lcddev.height/2-38+(i*11),lcddev.width/2-38+(i*11)+30,lcddev.height/2-38+(i*11)+30); 
  }

 delay_ms(100);
 delay_us(100);
}

/*****************************************************************************
 * @name       :void Test_Circle(void)
 * @date       :2018-08-09 
 * @function   :circular display and fill test
								Display red,green,blue,yellow,pink circular boxes in turn,
								1500 milliseconds later,
								Fill the circular in red,green,blue,yellow and pink in turn
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Test_Circle(void)
{
	u8 i=0;
        //_printf("Test_Circle\r\n",0);
        //delay_ms(10);
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Clear(WHITE);
		LCD_Fill(0,0,20,lcddev.height-1,BLUE);
		POINT_COLOR=WHITE;
		Show_Str(2,8,WHITE,BLUE,"画",16,1);
		Show_Str(2,24,WHITE,BLUE,"圆",16,1);
		Show_Str(2,40,WHITE,BLUE,"填",16,1);
		Show_Str(2,56,WHITE,BLUE,"充",16,1);
	}
	else
	{
		DrawTestPage("画圆填充");
	}
	for (i=0; i<5; i++)  
		gui_circle(lcddev.width/2-23+(i*11),lcddev.height/2-23+(i*11),ColorTab[i],15,0);
	delay_ms(1500);	
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Fill(21,0,lcddev.width-1,lcddev.height-1,WHITE);
	}
	else
	{
		LCD_Fill(0,21,lcddev.width-1,lcddev.height-1,WHITE);
	}
	for (i=0; i<5; i++) 
	  	gui_circle(lcddev.width/2-23+(i*11),lcddev.height/2-23+(i*11),ColorTab[i],15,1);
	delay_ms(1500);
}

/*****************************************************************************
 * @name       :void English_Font_test(void)
 * @date       :2018-08-09 
 * @function   :English display test
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void English_Font_test(void)
{
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Clear(WHITE);
		LCD_Fill(0,0,20,lcddev.height-1,BLUE);
		POINT_COLOR=WHITE;
		Show_Str(2,8,WHITE,BLUE,"英",16,1);
		Show_Str(2,24,WHITE,BLUE,"文",16,1);
		Show_Str(2,40,WHITE,BLUE,"显",16,1);
		Show_Str(2,56,WHITE,BLUE,"示",16,1);
		Show_Str(22,12,BLUE,GREEN,"6X12:abcdeABCDE012345",12,0);
		Show_Str(22,24,BLUE,YELLOW,"6X12:~!@#$%^&*()_+{}:",12,1);
		Show_Str(22,40,BLUE,YELLOW,"8X16:abcdABCD0123",16,0);
		Show_Str(22,56,BLUE,YELLOW,"8X16:~!@#$%^&*()",16,1); 
		delay_ms(1500);
		LCD_Fill(21,0,lcddev.width-1,lcddev.height-1,BLACK);
		Show_Str(22,12,BLUE,GREEN,"6X12:abcdeABCDE012345",12,0);
		Show_Str(22,24,BLUE,YELLOW,"6X12:~!@#$%^&*()_+{}:",12,1);
		Show_Str(22,40,BLUE,YELLOW,"8X16:abcdABCD0123",16,0);
		Show_Str(22,56,BLUE,YELLOW,"8X16:~!@#$%^&*()",16,1); 	
	}
	else
	{
		DrawTestPage("英文显示");
		Show_Str(2,34,BLUE,GREEN,"6X12:abcdefg",12,0);
		Show_Str(2,46,BLUE,YELLOW,"6X12:ABCDEFG",12,1);
		Show_Str(2,58,BLUE,BRRED,"6X12:0123456",12,0);
		Show_Str(2,70,BLUE,YELLOW,"6X12:~!@#$%^",12,1);
		Show_Str(2,86,BLUE,GREEN,"8X16:abcde",16,0);
		Show_Str(2,102,BLUE,YELLOW,"8X16:ABCDE",16,1);
		Show_Str(2,118,BLUE,BRRED,"8X16:0123",16,0);
		Show_Str(2,134,BLUE,YELLOW,"8X16:~!@#",16,1); 
		delay_ms(1500);
		LCD_Fill(0,21,lcddev.width-1,lcddev.height-1,BLACK);	
		Show_Str(2,34,BLUE,GREEN,"6X12:abcdefg",12,0);
		Show_Str(2,46,BLUE,YELLOW,"6X12:ABCDEFG",12,1);
		Show_Str(2,58,BLUE,BRRED,"6X12:0123456",12,0);
		Show_Str(2,70,BLUE,YELLOW,"6X12:~!@#$%^",12,1);
		Show_Str(2,86,BLUE,GREEN,"8X16:abcde",16,0);
		Show_Str(2,102,BLUE,YELLOW,"8X16:ABCDE",16,1);
		Show_Str(2,118,BLUE,BRRED,"8X16:0123",16,0);
		Show_Str(2,134,BLUE,YELLOW,"8X16:~!@#",16,1);		
	}

}

/*****************************************************************************
 * @name       :void Test_Triangle(void)
 * @date       :2018-08-09 
 * @function   :triangle display and fill test
								Display red,green,blue,yellow,pink triangle boxes in turn,
								1500 milliseconds later,
								Fill the triangle in red,green,blue,yellow and pink in turn
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Test_Triangle(void)
{
	u8 i=0;
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Clear(WHITE);
		LCD_Fill(0,0,20,lcddev.height-1,BLUE);
		POINT_COLOR=WHITE;
		Show_Str(2,0,WHITE,BLUE,"三",16,1);
		Show_Str(2,16,WHITE,BLUE,"角",16,1);
		Show_Str(2,32,WHITE,BLUE,"形",16,1);
		Show_Str(2,48,WHITE,BLUE,"填",16,1);
		Show_Str(2,64,WHITE,BLUE,"充",16,1);
	}
	else
	{
		DrawTestPage("三角形填充");
	}
	for(i=0;i<5;i++)
	{
		POINT_COLOR=ColorTab[i];
		Draw_Triangel(lcddev.width/2-38+(i*11),lcddev.height/2-10+(i*11),lcddev.width/2-23-1+(i*11),lcddev.height/2-10-26-1+(i*11),lcddev.width/2-8-1+(i*11),lcddev.height/2-10+(i*11));
	}
	delay_ms(1500);	
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Fill(21,0,lcddev.width-1,lcddev.height-1,WHITE);
	}
	else
	{
		LCD_Fill(0,21,lcddev.width-1,lcddev.height-1,WHITE);
	}
	for(i=0;i<5;i++)
	{
		POINT_COLOR=ColorTab[i];
		Fill_Triangel(lcddev.width/2-38+(i*11),lcddev.height/2-10+(i*11),lcddev.width/2-23-1+(i*11),lcddev.height/2-10-26-1+(i*11),lcddev.width/2-8-1+(i*11),lcddev.height/2-10+(i*11));
	}
	delay_ms(1500);
}

/*****************************************************************************
 * @name       :void Chinese_Font_test(void)
 * @date       :2018-08-09 
 * @function   :chinese display test
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Chinese_Font_test(void)
{	
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Clear(WHITE);
		LCD_Fill(0,0,20,lcddev.height-1,BLUE);
		POINT_COLOR=WHITE;
		Show_Str(2,8,WHITE,BLUE,"中",16,1);
		Show_Str(2,24,WHITE,BLUE,"文",16,1);
		Show_Str(2,40,WHITE,BLUE,"显",16,1);
		Show_Str(2,56,WHITE,BLUE,"示",16,1);
		Show_Str(22,3,RED,YELLOW,"全动电子欢迎您",16,0);
		Show_Str(22,19,BLUE,YELLOW,"中文测试",24,1);
		Show_Str(22,47,BLACK,YELLOW,"字体测试",32,1);
		delay_ms(1500);
		LCD_Fill(21,0,lcddev.width-1,lcddev.height-1,BLACK);
		Show_Str(22,3,RED,YELLOW,"全动电子欢迎您",16,0);
		Show_Str(22,19,BLUE,YELLOW,"中文测试",24,1);
		Show_Str(22,47,WHITE,YELLOW,"字体测试",32,1);
	}
	else
	{
		DrawTestPage("中文显示");
		Show_Str(2,26,RED,YELLOW,"全动电子",16,0);
		Show_Str(2,42,BLUE,YELLOW,"中文",24,1);
		Show_Str(2,66,BLUE,YELLOW,"测试",24,1);
		Show_Str(2,90,BLACK,YELLOW,"字体",32,1);
		Show_Str(2,122,BLACK,YELLOW,"测试",32,1);
		delay_ms(1500);
		LCD_Fill(0,21,lcddev.width-1,lcddev.height-1,BLACK);
		Show_Str(2,26,RED,YELLOW,"全动电子",16,0);
		Show_Str(2,42,BLUE,YELLOW,"中文",24,1);
		Show_Str(2,66,BLUE,YELLOW,"测试",24,1);
		Show_Str(2,90,WHITE,YELLOW,"字体",32,1);
		Show_Str(2,122,WHITE,YELLOW,"测试",32,1);
	}
	delay_ms(1200);
}

/*****************************************************************************
 * @name       :void Pic_test(void)
 * @date       :2018-08-09 
 * @function   :picture display test
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Pic_test(void)
{
/*	
  u8 i=0,j=0;
	if(lcddev.dir==1||lcddev.dir==3)
	{
		LCD_Clear(WHITE);
		LCD_Fill(0,0,20,lcddev.height-1,BLUE);
		POINT_COLOR=WHITE;
		Show_Str(2,8,WHITE,BLUE,"图",16,1);
		Show_Str(2,24,WHITE,BLUE,"片",16,1);
		Show_Str(2,40,WHITE,BLUE,"显",16,1);
		Show_Str(2,56,WHITE,BLUE,"示",16,1);
		for(i=0;i<2;i++)
		{
			for(j=0;j<3;j++)
			{
				Gui_Drawbmp16(30+j*40,i*40,gImage_qq);
			}
		}		
	}
	else
	{
		DrawTestPage("图片显示");
		for(i=0;i<3;i++)
		{
			for(j=0;j<2;j++)
			{
				Gui_Drawbmp16(j*40,30+i*40,gImage_qq);
			}
		}		
	}
	delay_ms(1200);
        */
}

/*****************************************************************************
 * @name       :void Rotate_Test(void)
 * @date       :2018-08-09 
 * @function   :rotate test
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Rotate_Test(void)
{

  u8 i=0;
    u8 *Direction[4]={"R:0","R:90","R:180","R:270"};
    for(i=0;i<4;i++)
    {
            LCD_direction(i);
            DrawTestPage("屏幕旋转");
            Show_Str(2,21,BLUE,YELLOW,Direction[i],16,1);
            //Gui_Drawbmp16((lcddev.width-40)/2,(lcddev.height-40)/2+5,gImage_qq);
            delay_ms(1000);delay_ms(1000);
    }
    LCD_direction(USE_HORIZONTAL);
}

/*****************************************************************************
 * @name       :void Touch_Test(void)
 * @date       :2018-08-09 
 * @function   :touch test
 * @parameters :None
 * @retvalue   :None
******************************************************************************/
void Touch_Test(void)
{
/*
  u8 key;
	u16 i=0;
	u16 j=0;
	u16 colorTemp=0;
	TP_Init();
	KEY_Init();
	LED_Init();
	DrawTestPage("测试9:Touch(按KEY0校准)      ");
	LCD_ShowString(lcddev.width-24,0,16,"RST",1);//显示清屏区域
	POINT_COLOR=RED;
	LCD_Fill(lcddev.width-52,2,lcddev.width-50+20,18,RED); 
		while(1)
	{
	 	key=KEY_Scan();
		tp_dev.scan(0); 		 
		if(tp_dev.sta&TP_PRES_DOWN)			//触摸屏被按下
		{	
		 	if(tp_dev.x<lcddev.width&&tp_dev.y<lcddev.height)
			{	
				if(tp_dev.x>(lcddev.width-24)&&tp_dev.y<16)
				{
					DrawTestPage("测试9:Touch(按KEY0校准)      ");//清除
					LCD_ShowString(lcddev.width-24,0,16,"RST",1);//显示清屏区域
					POINT_COLOR=colorTemp;
					LCD_Fill(lcddev.width-52,2,lcddev.width-50+20,18,POINT_COLOR); 
				}
				else if((tp_dev.x>(lcddev.width-60)&&tp_dev.x<(lcddev.width-50+20))&&tp_dev.y<20)
				{
				LCD_Fill(lcddev.width-52,2,lcddev.width-50+20,18,ColorTab[j%5]); 
				POINT_COLOR=ColorTab[(j++)%5];
				colorTemp=POINT_COLOR;
				delay_ms(10);
				}

				else TP_Draw_Big_Point(tp_dev.x,tp_dev.y,POINT_COLOR);		//画图	  			   
			}
		}else delay_ms(10);	//没有按键按下的时候 	    
		if(key==1)	//KEY_RIGHT按下,则执行校准程序
		{

			LCD_Clear(WHITE);//清屏
		    TP_Adjust();  //屏幕校准 
			TP_Save_Adjdata();	 
			DrawTestPage("测试9:Touch(按KEY0校准)      ");
			LCD_ShowString(lcddev.width-24,0,16,"RST",1);//显示清屏区域
			POINT_COLOR=colorTemp;
			LCD_Fill(lcddev.width-52,2,lcddev.width-50+20,18,POINT_COLOR); 
		}
		i++;
		if(i==30)
		{
			i=0;
			LED0=!LED0;
			//break;
		}
	}
*/   
}




