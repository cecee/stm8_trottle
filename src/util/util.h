#ifndef UTIL_H
#define UTIL_H

#include "stm8s.h"

#define xDEBUG
#ifndef BIT 
#define BIT(x) (1<<(x)) 
#endif 

#define u8  unsigned char
#define u16 unsigned int
#define u32 unsigned long

#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사

typedef struct 
{
  u32 msec;
  u16 beep_ms;
  u16 beep_period_ms;
  u16 beep_on_ms;
  u16 trottle;
  u16 volume;
  u16 pwm;
  u8  beep_status;
  u8  relay;
  u8  dir;
  u8  stop;
  u8 seqStatus;
}CURRENT_STATUS;

extern CURRENT_STATUS gValue;

void Udelay(u32 dd);
void Delay(int dd);
void delay(u32 dly); 
//void delay_ms(u16 dly);
void CUartTxChar(u8 ucValue);
void _printf(u8 *pFmt, u32 wVal);
long _sprintf(char *buf, char *format, long arg);
char *itoa( char *a, int i);
#endif