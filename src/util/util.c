#include "util.h"
#include "stm8s_uart1.h"
#include <string.h>
/*
void Delay(int dd)
{
int i,j;
for(i=0;i<dd;i++){for(j=0;j<1562;j++){}}
}

void Udelay(u32 dd)
{
  u32 i,j;
  for(i=0;i<dd;i++){for(j=0;j<3;j++){}
	}
}


void delay_ms(u16 dly)
{
	u16 i;
  for (i = 0; i < dly; i++) delay(108); // org 108=1ms
 //for (i = 0; i < 1000; i++); //1000:about 90uS
}
*/
char *itoa( char *a, int i)
{
	int sign=0;
	char buf[6];
	char *ptr;

	ptr = buf;
	/* zero then return */
	if( i )
	{
		/* make string in reverse form */
		if( i < 0 )	{ i = ~i + 1; sign++; }
		while( i )
			{ *ptr++ = (i % 10) + '0'; i = i / 10;}
		if( sign )*ptr++ = '-';
		*ptr = '\0';
		/* copy reverse order */
		for( i=0; i < strlen(buf); i++ )
			*a++ = buf[strlen(buf)-i-1];
	}	
	else	*a++ = '0';
	return a;
}


void CUartTxChar(u8 ucValue)
{
  UART1_SendData8(ucValue); //udelay(1);
	while(!(UART1->SR&0x40));
}

void _printf(u8 *pFmt, u32 wVal)
{

	u8 SendData;
	while (SendData=*(pFmt++))
	{
		if (SendData==(char)'%')
		{
			SendData=*(pFmt++);
			if(SendData==(char)'d' || SendData==(char)'x')
			{
				if(wVal)
				{
					u32 divider=10000;
					u8 dispValue,tmp;
					
					if (SendData==(char)'x')
						divider=0x1000;
					while(divider)
					{
						dispValue=wVal/divider;
						wVal-=(dispValue*divider);
						//if(dispValue)
						{
							if(dispValue>9)
								tmp = dispValue + ((char)'A'-10);
							else
								tmp = dispValue + (char)'0';
							CUartTxChar(tmp);
						}
						if(SendData==(char)'d')
							divider/=10;
						else
							divider/=0x10;
					}
				}
				else
					CUartTxChar('0');
				
			}
		}
		else
			CUartTxChar(SendData);
	}
}

long _sprintf(char *buf, char *format, long arg)
{
	char *start=buf;
	long *argp=(long *)&arg;
	char *p;
	
	//*start=buf;
	
  while( *format )
	{
		if( *format != '%' )	/* 일반적인 문자 */
		{
			*buf++ = *format++;
			continue;
		}

		format++;				/* skip '%' */

		if( *format == '%' )	/* '%' 문자가 연속 두번 있는 경우 */
		{
			*buf++ = *format++;
			continue;
		}
		
		switch( *format )
		{

			case 'c' :
				*buf++ = *(char *)argp++;
				break;
			case 'd' :
				buf = itoa(buf,*(int *)argp++);
				break;
			case 'x' : 
				//buf = xtoa(buf,*(unsigned int *)argp++,'a');
				break;
			case 'X' : 
				//buf = xtoa(buf,*(unsigned int *)argp++,'A');
				break;
	
			case 's' :
				p=*(char **)argp++;
				while(*p) 
						*buf++ = *p++;
				break;

			default :
				*buf++ = *format; /* % 뒤에 엉뚱한 문자인 경우 */
				break;
        }

		format++;
   }
    *buf = '\0';
   //return (long)buf;
   return(buf-start);
}

