#ifndef __SPI_H
#define __SPI_H	
#include  "stm8s.h"

void SPI0_Init(void);
void SPI_WriteByte(unsigned char Data);
unsigned char SPI_WriteReadByte(unsigned char Data);
 
#endif
