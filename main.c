#include "include.h"

u32 ms_tick=0;
u32 ms_cnt=0;

void CLK_Configuration(void)
{
  // Fmaster = 16MHz 
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  //CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
}

void GPIO_Initial(void)
{
  GPIO_DeInit(GPIOA);
  //GPIO_Init(GPIOA,GPIO_PIN_3,GPIO_MODE_IN_PU_IT);

  GPIO_DeInit(GPIOB);
  GPIO_Init(GPIOB,LCD_CS,GPIO_MODE_OUT_PP_HIGH_FAST);  
  GPIO_Init(GPIOB,LCD_RS,GPIO_MODE_OUT_PP_HIGH_FAST);     

  GPIO_DeInit(GPIOC);
  GPIO_Init(GPIOC,LCD_RST,GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOC,LCD_BLK,GPIO_MODE_OUT_PP_HIGH_FAST);
}

void Timer_Init(void)
{
  TIM4->SR1 = 0;       // clear overflow flag
  TIM4->PSCR = 6;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
  TIM4->ARR = 24;    // 25*4 = 100 us. 10KHZ
  TIM4->IER = 0x01;   // Enable interrupt
  TIM1->PSCRH = 0x00;  // 8M  f=fck/(PSCR+1)
  TIM1->PSCRL = 0x07; // PSCR=0x1F3F��f=8M/(0x1F3F+1)=1000Hz��Period : 1ms
  TIM4->CR1 = 0x01;   // Start timer
  
  TIM1->ARRH = 0x4E;  // 0x4e20=20000us=20ms
  TIM1->ARRL = 0x20;  // 
  TIM1->IER = 0x01;   // Enable interrupt
  TIM1->CR1 = 0x01;   // Start timer
}

main()
{
	CLK_Configuration();
	GPIO_Initial();
        
        UART1_DeInit(); // Initialize serial
        UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
        UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
        Timer_Init();
        SPI0_Init(); 
        enableInterrupts();
        
        LCD_BLK_SET;
        LCD_Init();
        LCD_Clear(YELLOW);
        LCD_direction(0);
 	while (1)
	{
          test_gui();
          delay_ms(4000);
 	}
}

#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  u8 data;
  data = UART1_ReceiveData8();
  _printf("Rcv [%x]\r\n",data);
}


#pragma vector = 25
__interrupt void _100us_ISR(void)
{
  static u32 t4=0;
  TIM4->SR1 = 0;      // clear overflow flag
  t4++;
  if(t4>10){
    t4=0;
    ms_tick++;
    ms_cnt++;
  }
}
/////////////////////////
//10ms Timer
/////////////////////////
#pragma vector=0xD
__interrupt void _10ms_ISR(void)
{
  static u32 t1=0;
  t1++;  
  TIM1->SR1 = 0;
   if(t1>100){
    _printf("ms_tick[%d]\r\n",ms_tick);
    t1=0;
  } 
}
