/************************************************************
*	ProjectName:	   LT86104EX
*	FileName:	       main.h
*	BuildData:	     2013-01-05
*	Version£º        V3.20
* Company:	       Lontium
************************************************************/

#ifndef		_MAIN_H
#define		_MAIN_H

extern  void  GPIO_Initial(void);
extern  void  CLK_Configuration(void);
#endif