/************************************************************
*	ProjectName:	   LT8522EX
*	FileName:	       lt8522ex.c
*	BuildData:	     2014-03-27
*	Version£º        V1.00
* Company:	       Lontium
************************************************************/

#ifndef		_INCLUDE_H
#define		_INCLUDE_H


#include "stm8s.h"
#include "main.h"
#include "string.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "../src/util/util.h"
#include "../src/util/delay.h"
#include "../src/spi/spi.h"
#include "../src/lcd/lcd.h"
#include "../src/gui/gui.h"
#include "../src/test/test.h"

extern u16 POINT_COLOR;// = 0x0000,BACK_COLOR = 0xFFFF;  
extern u16 BACK_COLOR;
extern u32 ms_tick;
extern u32 ms_cnt;
#endif